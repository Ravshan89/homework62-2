import React, { Component } from 'react';
import './number.css'

class Number extends Component {

    state = {
        numbers: this.getRandomArrayElements(5, 36)
    };
    getRandomArrayElements(min, max) {
        let randomNumberArray = [];
        for (var i = 0; randomNumberArray.length <= 4; i++) {
            let randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
            if (randomNumberArray.indexOf(randomNumber) > -1)
                continue;
            randomNumberArray.push(randomNumber)
        }
        return randomNumberArray.sort(function (a, b) {
            return a - b;
        });
    }
    changeRandomNumbers = () => {
        this.setState({
            numbers: this.getRandomArrayElements(5, 36)
        })
    }

    render() {
        return (
            <div className="container">
                <input type="button" value="Change Numbers" onClick={this.changeRandomNumbers} />
                <div className="numbers">
                    <div className="number">
                        <p>{this.state.numbers[0]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[1]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[2]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[3]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[4]}</p>
                    </div>
                </div>
            </div>
        );
    }
};
export default Number;
